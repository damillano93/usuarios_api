// importar biblioteca express
var express = require('express');
// importar biblioteca body-parser
var bodyParser = require('body-parser');
// importar biblioteca morgan
var logger = require('morgan')
// importar router facultad
var facultad = require('./routes/facultad');
// importar router rol
var rol = require('./routes/rol');
// importar router programa
var programa = require('./routes/programa');
// importar router usuarios
var usuarios = require('./routes/usuarios');

var cors = require('cors');
// importar router de status
var status = require('./routes/status'); 
// uso biblioteca express
var app = express();
//visualizacion respuestas en consola
app.use(logger('dev'))
//agrega CORS para tener accesso desde otras aplicaciones
app.use(cors());
// uso biblioteca mongoose
var mongoose = require('mongoose');
// conexion DB 
var dev_db_url = 'mongodb://root:example@172.24.0.3:27017/labs?authSource=admin';
mongoose.connect(dev_db_url);
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
// uso de respuestas y llamados en JSON
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
//creacion ruta /facultad
app.use('/facultad', facultad);
//creacion ruta /rol
app.use('/rol', rol);
//creacion ruta /programa
app.use('/programa', programa);
//creacion ruta /usuarios
app.use('/usuarios', usuarios);

//creacion ruta /
app.use('/', status);
// puerto de servicio
var port = 3003;
// inicir la conexion
app.listen(port, () => {
    console.log('Server in port ' + port);
});