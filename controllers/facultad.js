//importar modelo
var Facultad = require('../models/facultad');

//funcion create
exports.facultad_create = function (req, res) {
    var facultad = new Facultad(
        req.body
    );

    facultad.save(function (err, facultad) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
            
        }
        res.send({status: "created", id: facultad._id})
    })
};
//funcion read by id
exports.facultad_details = function (req, res) {
    Facultad.findById(req.params.id, function (err, facultad) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send(facultad);
    })
};
//funcion read all
exports.facultad_all = function (req, res) {
    Facultad.find(req.params.id, function (err, facultad) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send(facultad);
    })
};
//funcion update
exports.facultad_update = function (req, res) {
    Facultad.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, facultad) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send({status: "updated", facultad: facultad });
    });
};

//funcion delete
exports.facultad_delete = function (req, res) {
    Facultad.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send({status:"deleted"});
    })
};