//importar modelo
var Programa = require('../models/programa');

//funcion create
exports.programa_create = function (req, res) {
    var programa = new Programa(
        req.body
    );

    programa.save(function (err, programa) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
            
        }
        res.send({status: "created", id: programa._id})
    })
};
//funcion read by id
exports.programa_details = function (req, res) {
    Programa.findById(req.params.id, function (err, programa) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send(programa);
    })
};
//funcion read all
exports.programa_all = function (req, res) {
    Programa.find(req.params.id, function (err, programa) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send(programa);
    })
};
//funcion update
exports.programa_update = function (req, res) {
    Programa.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, programa) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send({status: "updated", programa: programa });
    });
};
//funcion add facultad
exports.Programa_add_facultad = function (req, res) {
Programa.findByIdAndUpdate(req.params.id, { $push: { facultad: req.body.facultad } }, { new: true }, function (err, Programa) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Programa: Programa });
});
};

//funcion delete
exports.programa_delete = function (req, res) {
    Programa.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send({status:"deleted"});
    })
};