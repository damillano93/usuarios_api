//importar modelo
var Usuarios = require('../models/usuarios');

//funcion create
exports.usuarios_create = function (req, res) {
    var usuarios = new Usuarios(
        req.body
    );

    usuarios.save(function (err, usuarios) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
            
        }
        res.send({status: "created", id: usuarios._id})
    })
};
//funcion read by id
exports.usuarios_details = function (req, res) {
    Usuarios.findById(req.params.id, function (err, usuarios) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send(usuarios);
    })
};
//funcion read all
exports.usuarios_all = function (req, res) {
    Usuarios.find(req.params.id, function (err, usuarios) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send(usuarios);
    })
};
//funcion update
exports.usuarios_update = function (req, res) {
    Usuarios.findByIdAndUpdate(req.params.id, {$set: req.body}, {new: true}, function (err, usuarios) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send({status: "updated", usuarios: usuarios });
    });
};
//funcion add programa
exports.Usuarios_add_programa = function (req, res) {
Usuarios.findByIdAndUpdate(req.params.id, { $push: { programa: req.body.programa } }, { new: true }, function (err, Usuarios) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Usuarios: Usuarios });
});
};
//funcion add rol
exports.Usuarios_add_rol = function (req, res) {
Usuarios.findByIdAndUpdate(req.params.id, { $push: { rol: req.body.rol } }, { new: true }, function (err, Usuarios) {
if (err) {
res.send({ status: "Err", error: err })
return next(err);
}
res.send({ status: "updated", Usuarios: Usuarios });
});
};

//funcion delete
exports.usuarios_delete = function (req, res) {
    Usuarios.findByIdAndRemove(req.params.id, function (err) {
        if (err) {
            res.send({status: "Err", error:err})
            console.log(err)
        }
        res.send({status:"deleted"});
    })
};