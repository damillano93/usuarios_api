//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Facultad
var FacultadSchema = new Schema({
nombre: {type:String, required:true}, 
ubicacion: {type:String, required:true}, 
descripcion: {type:String, required:true}, 

});

FacultadSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Facultad', FacultadSchema);