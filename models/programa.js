//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Programa
var ProgramaSchema = new Schema({
nombre: {type:String, required:true}, 
facultad: {type:Schema.Types.ObjectId, ref: 'Facultad' , autopopulate: true }, 

});

ProgramaSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Programa', ProgramaSchema);