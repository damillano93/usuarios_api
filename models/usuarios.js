//importar bilbioteca mongoose
var mongoose = require('mongoose');
//importar bilbioteca Schema
var Schema = mongoose.Schema;
// creacion esquema de Usuarios
var UsuariosSchema = new Schema({
nombre: {type:String, required:true}, 
codigo_usuario: {type:String, required:true}, 
email: {type:String, required:true}, 
programa: {type:Schema.Types.ObjectId, ref: 'Programa' , autopopulate: true }, 
rol: {type:Schema.Types.ObjectId, ref: 'Rol' , autopopulate: true }, 
estado: {type:String, required:true}, 

});

UsuariosSchema.plugin(require('mongoose-autopopulate'));
// Exportar modelo
module.exports = mongoose.model('Usuarios', UsuariosSchema);