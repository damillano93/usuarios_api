// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de facultad
var facultad_controller = require('../controllers/facultad');

// GET /:id
router.get('/:id', facultad_controller.facultad_details);
// GET /
router.get('/', facultad_controller.facultad_all);
// POST /
router.post('/', facultad_controller.facultad_create);
// PUT /:id
router.put('/:id', facultad_controller.facultad_update);
// DELETE /:id
router.delete('/:id', facultad_controller.facultad_delete);

//export router
module.exports = router;