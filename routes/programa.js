// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de programa
var programa_controller = require('../controllers/programa');

// GET /:id
router.get('/:id', programa_controller.programa_details);
// GET /
router.get('/', programa_controller.programa_all);
// POST /
router.post('/', programa_controller.programa_create);
// PUT /:id
router.put('/:id', programa_controller.programa_update);
// DELETE /:id
router.delete('/:id', programa_controller.programa_delete);
// PUT facultad 
router.put('/facultad/:id',programa_controller.Programa_add_facultad);

//export router
module.exports = router;