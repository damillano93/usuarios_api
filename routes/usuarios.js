// importar biblioteca express
var express = require('express');
// crear router
var router = express.Router();

// importar controlador de usuarios
var usuarios_controller = require('../controllers/usuarios');

// GET /:id
router.get('/:id', usuarios_controller.usuarios_details);
// GET /
router.get('/', usuarios_controller.usuarios_all);
// POST /
router.post('/', usuarios_controller.usuarios_create);
// PUT /:id
router.put('/:id', usuarios_controller.usuarios_update);
// DELETE /:id
router.delete('/:id', usuarios_controller.usuarios_delete);
// PUT rol 
router.put('/rol/:id',usuarios_controller.Usuarios_add_rol);

//export router
module.exports = router;