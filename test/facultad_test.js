var superagent = require('superagent')
var expect = require('expect.js')

describe('Test facultad', function () {
  var id
  var url = 'http://localhost:3003';


  // ------------------Pruebas a la ruta disponibilidad------------------

  // POST /facultad
  it('Test POST /facultad ', function (done) {
    superagent.post(url + '/facultad')
      .send({
        nombre: "Educacion",
        ubicacion: "Macarena",
        descripcion: "educacion Bogota"
        
      })
      .end(function (e, res) {
        id = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // GET /facultad
  it('Test GET /facultad', function (done) {
    superagent.get(url + '/facultad')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })


 // GET /facultad/:id
 it('test GET /facultad/:id', function (done) {
  superagent.get(url + '/facultad/'+id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body._id).to.eql(id)
      done()
    })
})
// PUT /facultad/:id
it('Test PUT /facultad/:id ', function (done) {
  superagent.put(url + '/facultad/'+id)
    .send({
      nombre: "Educacion_prueba",
      ubicacion: "Macarena_prueba",
      descripcion: "educacion Bogota_prueba"
    })
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(res.body.status).to.eql('updated')
      expect(res.body.disponibilidad.estado).to.eql('inactivo')
      done()
    })
})

// DELETE /facultad/:id
it('Test DELETE /facultad/:id ', function (done) {
  superagent.delete(url +'/facultad/' + id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body.status).to.eql('deleted')
      done()
    })
})

})
