var superagent = require('superagent')
var expect = require('expect.js')

describe('Test programa', function () {
  var id
  var url = 'http://localhost:3003';


  // ------------------Pruebas a la ruta sede------------------

  // POST /programa
  it('Test POST /programa ', function (done) {
    superagent.post(url + '/programa')
      .send({
        nombre: "idiomas" , 
        descripcion:"educacion" , 
        
      })
      .end(function (e, res) {
        id = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // GET /programa
  it('Test GET /programa', function (done) {
    superagent.get(url + '/programa')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })


 // GET /programa/:id
 it('test GET /programa/:id', function (done) {
  superagent.get(url + '/programa/'+id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body._id).to.eql(id)
      done()
    })
})
// PUT /programa/:id
it('Test PUT /programa/:id ', function (done) {
  superagent.put(url + '/programa/'+id)
    .send({
      nombre: "ciencias" , 
      descripcion:"Facultad de ingenieria" , 
      direccion: "calle 40"
    })
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(res.body.status).to.eql('updated')
      expect(res.body.sede.nombre).to.eql('Ciencias')
      done()
    })
})

// DELETE /programa/:id
it('Test DELETE /programa/:id ', function (done) {
  superagent.delete(url +'/programa/' + id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body.status).to.eql('deleted')
      done()
    })
})

})
