var superagent = require('superagent')
var expect = require('expect.js')

describe('Test Laboratorio', function () {
  var id
  var id_tipo
  var id_ubicacion
  var id_sede
  var id_disponibilidad
  var url = 'http://localhost:3001';
  // GET /
  it('probar status API', function (done) {
    superagent.get(url)
      .end(function (e, res) {
        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        expect(res.body.status).to.eql('Ok')
        done()
      })
  })

  // ------------------Pruebas a la ruta laboratorio------------------
   // POST /tipo
   it('Test POST /tipo ', function (done) {
    superagent.post(url + '/tipo')
      .send({
        nombre: "Electronica" , 
        descripcion:"Laboratorio de tipo electronica" 
      })
      .end(function (e, res) {
        id_tipo = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // POST /ubicacion
  it('Test POST /ubicacion ', function (done) {
    superagent.post(url + '/ubicacion')
      .send({
        nombre: "Piso 5" , 
        descripcion:"Laboratoriol piso 5",
        direccion: "Calle 123"
      })
      .end(function (e, res) {
        id_ubicacion = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // POST /sede
  it('Test POST /sede ', function (done) {
    superagent.post(url + '/sede')
      .send({
        nombre: "Ingenieria" , 
        descripcion:"Facultad de ingenieria" , 
        direccion: "calle 40"
      })
      .end(function (e, res) {
        id_sede = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  it('Test POST /disponiblidad ', function (done) {
    superagent.post(url + '/disponibilidad')
      .send({
        fecha: "23/05/19",
        hora: "9:00",
        estado: "activo",
        responsable: "123456"
      })
      .end(function (e, res) {
        id_disponibilidad = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // POST /laboratorio
  it('Test POST /laboratorio ', function (done) {
    superagent.post(url + '/laboratorio')
      .send({
        nombre: "Fisica 2", 
        estado: "Activo", 
        tipo: id_tipo, 
        ubicacion: id_ubicacion, 
        sede: id_sede, 
        disponibilidad: id_disponibilidad

      })
      .end(function (e, res) {
        id = res.body.id;
        expect(e).to.eql(null)
        expect(res.body.status).to.eql('created')
        expect(res.body.id).to.be.an('string')
        done()
      })
  })
  // GET /laboratorio
  it('Test GET /laboratorio', function (done) {
    superagent.get(url + '/laboratorio')
      .end(function (e, res) {

        expect(e).to.eql(null)
        expect(typeof res.body).to.eql('object')
        done()
      })
  })


 // GET /laboratorio/:id
 it('test GET /laboratorio/:id', function (done) {
  superagent.get(url + '/laboratorio/'+id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body._id).to.eql(id)
      done()
    })
})
// PUT /laboratorio/:id
it('Test PUT /laboratorio/:id ', function (done) {
  superagent.put(url + '/laboratorio/'+id)
    .send({
      nombre: "Piso 6" , 
        descripcion:"Laboratoriol piso 6",
        direccion: "Calle 123"
    })
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(res.body.status).to.eql('updated')
      expect(res.body.laboratorio.nombre).to.eql('Piso 6')
      done()
    })
})

// DELETE /laboratorio/:id
it('Test DELETE /laboratorio/:id ', function (done) {
  superagent.delete(url +'/laboratorio/' + id)
    .end(function (e, res) {
      expect(e).to.eql(null)
      expect(typeof res.body).to.eql('object')
      expect(res.body.status).to.eql('deleted')
      done()
    })
})

})
